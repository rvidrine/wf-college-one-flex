<?PHP get_header(); ?>
<?php get_sidebar('primary'); ?>
<?php get_sidebar('secondary'); ?>

 <div id="mainContent">
    <div class="wfCollegeOne">
  <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <div class="entry-content">
      <?php the_title('<h1 class="page-title">', '</h1>'); ?>
        <p class="posted_date">
          Posted on: <?php the_time(get_option('date_format')); ?>
        </p>
      <div class="dept-news-item">
        <?php	the_content(); ?>
        <p class="newstypes">
          <?php echo get_the_term_list( $post->ID, 'news_type', 'Related News: ', ' &bull; ', '' ); ?>
        </p>
      </div>
    </div>
                      
    </div>
</div>
<div class="navigation">
	    <div class="alignleft"><?php next_posts_link('Previous news item') ?></div>
	    <div class="alignright"><?php previous_posts_link('Next news item') ?></div>
	</div>

<?php endwhile; ?>
<?PHP get_footer(); ?>
