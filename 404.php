<?PHP get_header(); ?>
<?php get_sidebar('primary'); ?>

 <div id="mainContent">
    <div class="wfCollegeOne">
<h1><strong>I'm sorry, this page was not found.</strong></h1>
<p>There may be a problem with the link you followed, or you may have made an error in typing the address.
Please try a link from the navigation menu, or use the search box to find the page you were looking for.<p>
<!-- <p>If you received this page after following a link on this site, please contact this site's administrator at 123@wfu.edu -->
                        
    </div>
</div>

<?PHP get_footer(); ?>
