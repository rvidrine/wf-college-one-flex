<div id="sidebar-secondary" class="sidebar">
  <p class="news-sidebar-title">Department News</p>
  <?php $args = array( 'post_type' => 'wfco_dept_news', 'posts_per_page' => 5 );
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post(); ?>
      <div class="entry-content">
      <p class="news-teaser"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
        <?php the_title(); ?></a></p>
      </div>
  <?php endwhile; ?>
  <h4>
    <a href="news">News Archive</a>
  </h4>

</div>
