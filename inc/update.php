<?php
/* From http://wordpress.stackexchange.com/questions/123307/how-do-i-use-the-http-request-host-is-external-filter
* to register my server as an authorized update server
*/
	
add_filter( 'http_request_host_is_external', 'allow_capeville_update_host', 10, 3 );
function allow_capeville_update_host( $allow, $host, $url ) {
  if ( $host == 'vidrinmr.capeville.wfunet.wfu.edu' )
    $allow = true;
  return $allow;
}

/*
// TEMP: Enable update check on every request. Normally you don't need this! This is for testing only!
set_site_transient('update_themes', null);
*/

add_filter('pre_set_site_transient_update_themes', 'check_for_update');

function check_for_update($checked_data) {
	global $wp_version;
	
	if (empty($checked_data->checked))
		return $checked_data;
	
	$api_url = 'http://vidrinmr.capeville.wfunet.wfu.edu/wfcollegeonepro_home/';
	$theme_base = basename(dirname(dirname(__FILE__)));
	
	$request = array(
		'slug' => $theme_base,
		'version' => $checked_data->checked[$theme_base] // simplified this line from original
	);
	
	// Start checking for an update
	$send_for_check = array(
		'body' => array(
			'action' => 'theme_update', 
			'request' => serialize($request),
			'api-key' => md5(get_bloginfo('url'))
		),
		'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
	);

	$raw_response = wp_remote_post($api_url, $send_for_check);
	
	if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200))
		$response = unserialize($raw_response['body']);
		
	// Feed the update data into WP updater
	if (!empty($response)) 
		$checked_data->response[$theme_base] = $response;
	
	return $checked_data;
}


if (is_admin())
	$current = get_transient('update_themes');

?>