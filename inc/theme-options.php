<?php 

	
	// Theme customization stuff
	 
	if (! function_exists( 'wfcopro_customize_register' ) ) { 
		function wfcopro_customize_register( $wp_customize )
		{
			// **** Color Schemes including container color, body color and menu color
			// **** End of Color Schemes
			
			// **** DD Smooth Menu styling
			$wp_customize->add_setting( 'ddsm_default_color' , array( 
			//create a setting for the color of the menu at rest
				'default'		=> '#C1B58A',
				'transport'	=> 'refresh',
			) );
			$wp_customize->add_setting( 'ddsm_hover_color' , array( 
			//create a setting for the color of the menu during mouseover
				'default'		=> '#E7DCBA',
				'transport'	=> 'refresh',
			) );
			$wp_customize->add_setting( 'ddsm_text_color' , array( 
			//create a setting for the text color of the menu
				'default'		=> '#000000',
				'transport'	=> 'refresh',
			) );
			/* Need to figure out how to handle menu code positioning -- get to this later
			** The header-hnav.php actually puts the DDSM in a different place. Want to be
			** backwards-compatible as much as possible, and haven't figured best way to do this yet.
			$wp_customize->add_setting( 'ddsm_menu_direction' , array( 
				//DDSM menu direction, horizontal or vertical
				'default'     => 'v',
				'transport'   => 'refresh',
			) ); */
			$wp_customize->add_section( 'ddsmooth_styles' , array(
				// Add a section to organize all our DDSM settings
				'title'      => __('DD Smooth Menu Style','wfcopro'),
				'priority'   => 31,
			) );
 			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ddsm_default_color', array(
				'label'        => __( 'Menu Color', 'wfcopro' ),
				'section'    => 'ddsmooth_styles',
				'settings'   => 'ddsm_default_color',
			) ) );
 			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ddsm_hover_color', array(
				'label'        => __( 'Menu Hover Color', 'wfcopro' ),
				'section'    => 'ddsmooth_styles',
				'settings'   => 'ddsm_hover_color',
			) ) );
 			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ddsm_text_color', array(
				'label'        => __( 'Menu Text Color', 'wfcopro' ),
				'section'    => 'ddsmooth_styles',
				'settings'   => 'ddsm_text_color',
			) ) );
			/* Control for the menu direction - works, but other issues need to be resolved
			$wp_customize->add_control( 'ddsm_menu_direction', array(
				'label'      => __( 'Set DD Smooth Menu Direction', 'wfcopro' ),
				'section'    => 'ddsmooth_styles',
				'settings'   => 'ddsm_menu_direction',
				'type'       => 'radio',
				'choices'    => array(
					'h' => 'Horizontal',
					'v' => 'Vertical',
			) ) ); */

			// **** End of DD Smooth Menu code.
			
			// *** Styling the left and right sidebar areas
/* I'll get to this later
			$wp_customize->add_setting( 'sidebar_border_style' , array( //create a setting to show/hide the montage image
				'default'     => 'thin black solid',
				'transport'   => 'refresh',
			) );
*/
			// **** End of sidebar styling stuff
			// **** Style the title color
			$wp_customize->add_setting( 'title_color' , array( //create a setting for the title color
				'default'		=> '#000000',
				'transport'	=> 'refresh',
			) );
 			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'title_color', array(
				'label'        => __( 'Title Color', 'wfcopro' ),
				'section'    => 'title_tagline',
				'settings'   => 'title_color',
			) ) );
			//**** End of title color stuff (no need to create section, used existing ont)

			// **** Style the montage area, including show/hide and image selector
			$wp_customize->add_setting( 'display_montage_image' , array( //create a setting to show/hide the montage image
				'default'     => 'none',
				'transport'   => 'refresh',
			) );
			$wp_customize->add_setting( 'select_montage_image' , array( //create a setting to select the montage image
				'default'     => trailingslashit( get_template_directory_uri() ) . 'images/header_montage.jpg',
				'transport'   => 'refresh',
			) );
			$wp_customize->add_section( 'wfcopro_montage_image' , array(
				'title'      => __('Montage Image','wfcopro'),
				'priority'   => 30,
			) );
			$wp_customize->add_control( 'wfcopro_montage_image', array(
				'label'      => __( 'Show Hide Montage', 'wfcopro' ),
				'section'    => 'wfcopro_montage_image',
				'settings'   => 'display_montage_image',
				'type'       => 'radio',
				'choices'    => array(
					'none' => 'Hide it',
					'block' => 'Show it',
					),
			) );
 			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'select_montage_image', array(
				'label'        => __( 'Select Montage Image', 'wfcopro' ),
				'section'    => 'wfcopro_montage_image',
				'settings'   => 'select_montage_image',
			) ) );
			// **** End of the montage stuff
		}
		add_action( 'customize_register', 'wfcopro_customize_register' );
	}
	if (! function_exists( 'wfcopro_theme_customize_css' ) ) {
		function wfcopro_customize_css()
		{
			?>
				 <style type="text/css">
					#blogTitle { color:<?php echo get_theme_mod('title_color', '#000000'); ?>; }
					#montage { display: <?php echo get_theme_mod('display_montage_image', 'none'); ?>; 
						background-image: url(<?php echo get_theme_mod( 'select_montage_image', trailingslashit( get_template_directory_uri() ) . 'images/header_montage.jpg' ); ?>); }
					.wfCollegeOne #vertnav ul li a { background: <?php echo get_theme_mod( 'ddsm_default_color', '#C1B58A' ); ?> ;
						color: <?php echo get_theme_mod( 'ddsm_text_color', '#000000' ); ?> ; }
					.wfCollegeOne #vertnav ul li a:hover { background: <?php echo get_theme_mod( 'ddsm_hover_color', '#E7DCBA' ); ?> ; }
				 </style>
			<?php
		}
		add_action( 'wp_head', 'wfcopro_customize_css');
	}
	// End of theme customization stuff
?>