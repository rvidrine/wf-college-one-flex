<?php
  if ( is_active_sidebar('header') ) : { //if there are widgets ?>
  <div id="sidebar-header" class="sidebar">
    <ul>
      <li>
        <?php dynamic_sidebar(header); // display the widgets ?>
      </li>
    </ul>
  </div>
<?php  } else : { // do something if there aren't widgets ?> 
<!-- If you don't want anything to appear in the header widget area, copy sidebar-header.php into your child theme and delete the following div. -->
  <div id="sidebar-header" class="sidebar">
    <ul>
      <li>
        <a title="Wake Forest University Home" href="http://www.wfu.edu/">Wake Forest University</a>
      </li>
      <li>
        <a title="Wake Forest College of Arts and Sciences" href="http://college.wfu.edu/">Wake Forest College</a>
      </li>
      <li id="loginout_link">
        <?php wp_loginout( get_permalink(),1); ?>
      </li>
        <?php wp_register( '<li id="register_link">', '</li>',1); ?>
    </ul>
  </div>
<!-- Delete the above div if you want no content in your header widget area -->
<?php };
endif; ?>

